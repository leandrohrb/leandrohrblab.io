---
title: "About Me"
date: 2019-06-11
draft: false
---

First of all, **thanks for your interest**, it's a pleasure to write this blog.
I hope you find something useful or at least have some fun! In short, I'm a
Brazilian developer that really cares about **free software**. I want to write
about stuff (both technological and ideological) that I find interesting with
you. Feel free to use use the content of my posts and to contact me for
anything! Any criticism or correction is welcome.
